package university.jala.academic;

import university.jala.academic.enums.Suit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Deck {
    final ArrayList<Card> cards;
    private final Random random = new Random();

    public Deck() {
        cards = new ArrayList<>();
    } //list with the cards of the deck

    public void startDeck() {
        for (Suit suit : Suit.values()) {
            for (int i = 2; i <= 10; i++) {
                cards.add(new Card(suit, String.valueOf(i)));
            }
            cards.add(new Card(suit, "J"));
            cards.add(new Card(suit, "Q"));
            cards.add(new Card(suit, "K"));
            cards.add(new Card(suit, "A"));
        }

        Collections.shuffle(cards);//shuffle cards

    } //create randomly sorted decks

    public Card takeCard() {
        return cards.get(random.nextInt(cards.size()));
    } //take a random card
}
