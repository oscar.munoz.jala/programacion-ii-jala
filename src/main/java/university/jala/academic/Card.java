package university.jala.academic;

import university.jala.academic.enums.Suit;

public class Card {
    private final Suit suit;
    private final String value;

    public Card(Suit suit, String value) {
        this.suit = suit;
        this.value = value;
    }

    public Suit getSuit() {
        return suit;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return suit.getSymbol() + value ;
    } // {D,C,H,S} + {2,3,4,5,6,7,8,9,10,J,Q,K,A}
}
