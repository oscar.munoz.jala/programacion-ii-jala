package university.jala.academic;

public class Main {
    public static String result;
    public static Deck deck = new Deck();

    public static void main(String[] args) {
        deck.startDeck();
        gameStart();
    }

    public static Card randomCard(){
        return deck.takeCard();
    }; //get a random card for system and player

    public static int changeLetterForNumber(String letters) {
        return switch (letters) {
            case "J" -> 11;
            case "Q" -> 12;
            case "K" -> 13;
            case "A" -> 14;
            default -> Integer.parseInt(letters);
        };
    } //change {J,Q,K,A} for a number

    public static void gameStart(){

        Card playerCard = randomCard();
        Card systemCard = randomCard();

        int systemValue = changeLetterForNumber(systemCard.getValue());
        int playerValue = changeLetterForNumber(playerCard.getValue());

        System.out.println("\nCARD GAME\n");
        System.out.println("Cards:");
        System.out.println("-------------");
        System.out.println("Computer: " + systemCard.toString());
        System.out.println("Player: " + playerCard.toString());
        System.out.println("-------------");

        result = gameWinner(systemValue,playerValue);
        System.out.println(result);
        System.out.println("-------------");
    }

    public static String gameWinner(int systemValue, int playerValue){

        String result;
        if (playerValue == systemValue) {
            result = "Tie :O";
        } else if (playerValue > systemValue) {
            result = "You win :D";
        } else {
            result = "You lost :(";
        }
        return result;

    } //rules of the game, the player with the highest number wins.
}
