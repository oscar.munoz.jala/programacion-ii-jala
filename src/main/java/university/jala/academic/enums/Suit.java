package university.jala.academic.enums;

public enum Suit {
    DIAMONDS("D"),
    CLOVERS("C"),
    HEARTS("H"),
    SPADES("S");

    private final String symbol;

    public String getSymbol() {
        return symbol;
    }

    Suit(String symbol) {
        this.symbol = symbol;
    }
}
