package university.jala.academic;

import org.junit.Test;
import university.jala.academic.enums.Suit;
import static org.junit.Assert.assertEquals;
import static university.jala.academic.Main.*;

public class GameTest {

    @Test
    public void createNewCard(){
        Card card = new Card(Suit.CLOVERS,"1");
        assertEquals("C1",card.toString());
    }

    @Test
    public void startDeck(){

        Deck deck = new Deck();
        for (Suit suit : Suit.values()) {
            for (int i = 2; i <= 10; i++) {
                deck.cards.add(new Card(suit, String.valueOf(i)));
            }

            deck.cards.add(new Card(suit, "J"));
            deck.cards.add(new Card(suit, "Q"));
            deck.cards.add(new Card(suit, "K"));
            deck.cards.add(new Card(suit, "A"));

        }
        assertEquals(52,deck.cards.size()); // ((DIAMONDS | CLOVERS | HEARTS | SPADES) + {2,3,4,5,6,7,8,9,J,Q,K,A}) = 58 cards
    }

    @Test
    public void winner(){

        Card cardSystem = new Card(Suit.CLOVERS,"K");
        Card cardPlayer = new Card(Suit.CLOVERS,"A");

        int systemValue = changeLetterForNumber(cardSystem.getValue());
        int playerValue = changeLetterForNumber(cardPlayer.getValue());

        String result = gameWinner(systemValue,playerValue);

        assertEquals("You win :D",result);
    }
}
