# Card Game

This program is a mini game, where the machine will take a random card from the Deck and give us a card, both cards are compared and the winner is the one who has the card with the highest value.

## Output

![Output.png](Output.png)

## Workflow

```
---
title: Sample workflow
---
flowchart LR
start([start])
Enum Suit:
    DIAMONDS("D"),
    CLOVERS("C"),
    HEARTS("H"),
    SPADES("S");
class Card:
   Atributes --> Suit & Value ({2,3,4,5,6,7,8,9,10,J,Q,K,A})
   Constructor(suit, value)	       
class Desk:
   cards --> list of cards for the deck
   startDeck() --> create randomly sorted decks
   takeCard() --> take a random card of the deck.
Main:
   startDeck()
   gameStart() --> deck.takeCard() for the system and player  
                   changeLetterForNumber(): change {J,Q,K,A} for a number  
                   or keep the same number and store it in a variable
                   gameWinner(): compare the previous variables where:
		   
                   if player's card is greater: print "You win"
                   if the system card is greater: print "You lost"
                   if player's card is equal to the system card = print "Tie"
([end])

```

## Diagram

![Diagram.png](Diagram.png)

## Execution

![Execution.png](Execution.png)

## Tests

![Test.png](Test.png)